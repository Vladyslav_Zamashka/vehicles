CREATE TABLE IF NOT EXISTS public.vehicle (
  id serial NOT NULL,
  coordinate geometry(Point, 4326) NOT NULL,
  updated timestamp NOT NULL,
  PRIMARY KEY (id)
);

CREATE INDEX vehicle_geo_index ON vehicle USING GIST (coordinate);