# Vehicles

## Requirements

For building and running the application you need:

- [Java 8]
- [Maven]
- [Docker]

## Preparations

### DB
- run `docker-compose up` command in the .\DB folder

### 1. Application build
- `mvn clean package -DskipTests`

### 2. Start
- `java -jar vehicles-1.0-SNAPSHOT.jar` (in the `./target` folder)

### Testing
- `mvn clean test`

### Check application
- `http://localhost:8080` (swagger aveialble for the base URL)

## API
- POST /vehicle - create vehicle
- PUT /vehicle/id - update vehicle
- GET /vehicle?latitude1=0&longitude2=0&latitude1=0&longitude2=0 - search in rectangle polygon