package com.test.vehicles.service.impl;

import com.test.vehicles.model.converter.Converter;
import com.test.vehicles.model.dto.VehicleDTO;
import com.test.vehicles.model.entity.Vehicle;
import com.test.vehicles.repository.VehicleRepository;
import com.test.vehicles.service.GeometryService;
import com.test.vehicles.service.VehicleGeoService;

import com.vividsolutions.jts.geom.Polygon;

import lombok.AllArgsConstructor;

import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
@AllArgsConstructor
public class DefaultVehicleGeoService implements VehicleGeoService {

    private final GeometryService geometryService;
    private final VehicleRepository vehicleRepository;
    private final Converter<VehicleDTO, Vehicle> vehicleToVehicleDtoConverter;

    @Override
    public List<VehicleDTO> findVehiclesByPolygon(double latitude1, double longitude1, double latitude2, double longitude2) {
        Polygon polygon = geometryService.createRectanglePolygon(longitude1, longitude2, latitude1, latitude2);

        return vehicleRepository.findVehicles(polygon).stream()
                .map(vehicleToVehicleDtoConverter::convert)
                .collect(Collectors.toList());
    }
}
