package com.test.vehicles.service.impl;

import com.test.vehicles.service.GeometryService;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LinearRing;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.geom.Polygon;
import com.vividsolutions.jts.geom.PrecisionModel;
import com.vividsolutions.jts.geom.impl.CoordinateArraySequence;

import lombok.AllArgsConstructor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import static com.test.vehicles.c.C.SRID;
import static com.vividsolutions.jts.geom.Coordinate.X;
import static com.vividsolutions.jts.geom.Coordinate.Y;

@Component
@AllArgsConstructor
public class DefaultGeometryService implements GeometryService {
    private static final Logger LOG = LoggerFactory.getLogger(DefaultGeometryService.class);

    private final PrecisionModel precisionModel = new PrecisionModel();
    private final GeometryFactory geometryFactory = new GeometryFactory(precisionModel, SRID);

    @Override
    public Point createPoint(double longitude, double latitude) {
        Coordinate coordinate = createCoordinate(longitude, latitude);

        LOG.debug("Creating point with x: '{}', y: '{}' coordinates",
                coordinate.getOrdinate(X),
                coordinate.getOrdinate(Y));

        return geometryFactory.createPoint(coordinate);
    }

    @Override
    public Polygon createRectanglePolygon(double longitude1, double longitude2, double latitude1, double latitude2) {
        Coordinate[] points = createRectanglePolygonCoordinates(longitude1, longitude2, latitude1, latitude2);

        return geometryFactory.createPolygon(new LinearRing(new CoordinateArraySequence(points), geometryFactory), getEmptyLinearRingArray());
    }

    private Coordinate[] createRectanglePolygonCoordinates(double longitude1, double longitude2, double latitude1, double latitude2) {
        return new Coordinate[] {
                createCoordinate(longitude1, latitude1),
                createCoordinate(longitude1, latitude2),
                createCoordinate(longitude2, latitude2),
                createCoordinate(longitude2, latitude1),
                createCoordinate(longitude1, latitude1)
        };
    }

    private Coordinate createCoordinate(double longitude, double latitude) {
        return new Coordinate(longitude, latitude);
    }

    private LinearRing[] getEmptyLinearRingArray() {
        return new LinearRing[0];
    }
}
