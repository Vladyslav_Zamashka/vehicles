package com.test.vehicles.service.impl;

import com.test.vehicles.exception.VehicleNotFoundException;
import com.test.vehicles.model.converter.Converter;
import com.test.vehicles.model.dto.VehicleDTO;
import com.test.vehicles.model.entity.Vehicle;
import com.test.vehicles.repository.VehicleRepository;
import com.test.vehicles.service.GeometryService;
import com.test.vehicles.service.VehicleService;

import com.vividsolutions.jts.geom.Point;
import lombok.AllArgsConstructor;

import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
@AllArgsConstructor
public class DefaultVehicleService implements VehicleService {

    private final VehicleRepository vehicleRepository;
    private final GeometryService geometryService;
    private final Converter<VehicleDTO, Vehicle> vehicleToVehicleDtoConverter;
    private final Converter<Vehicle, VehicleDTO> vehicleDtoToVehicleConverter;

    @Override
    public VehicleDTO updateVehicle(long id, VehicleDTO vehicle) {
        Vehicle vehicleToUpdate = vehicleRepository.findById(id)
                .orElseThrow(() -> new VehicleNotFoundException(String.format("Vehicle with id '%o' not found", id)));

        vehicleToUpdate.setUpdated(LocalDateTime.now());

        Point vehicleCoordinate = geometryService.createPoint(vehicle.getLongitude(), vehicle.getLatitude());
        vehicleToUpdate.setVehicleCoordinate(vehicleCoordinate);

        return vehicleToVehicleDtoConverter.convert(vehicleRepository.saveAndFlush(vehicleToUpdate));
    }

    @Override
    public VehicleDTO saveVehicle(VehicleDTO vehicle) {
        Vehicle newVehicle = vehicleDtoToVehicleConverter.convert(vehicle);

        return vehicleToVehicleDtoConverter.convert(vehicleRepository.saveAndFlush(newVehicle));
    }
}
