package com.test.vehicles.service;

import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.geom.Polygon;

public interface GeometryService {
    
    Point createPoint(double longitude, double latitude);
    
    Polygon createRectanglePolygon(double longitude1, double longitude2, double latitude1, double latitude2);
}
