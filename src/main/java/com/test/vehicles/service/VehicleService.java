package com.test.vehicles.service;

import com.test.vehicles.model.dto.VehicleDTO;

public interface VehicleService {

    VehicleDTO updateVehicle(long id, VehicleDTO vehicle);

    VehicleDTO saveVehicle(VehicleDTO vehicle);

}
