package com.test.vehicles.service;

import com.test.vehicles.model.dto.VehicleDTO;

import java.util.List;

public interface VehicleGeoService {

    List<VehicleDTO> findVehiclesByPolygon(double latitude1, double longitude1, double latitude2, double longitude2);

}
