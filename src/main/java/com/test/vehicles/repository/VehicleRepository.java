package com.test.vehicles.repository;

import com.test.vehicles.model.entity.Vehicle;

import com.vividsolutions.jts.geom.Polygon;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface VehicleRepository extends JpaRepository<Vehicle, Long> {
    @Query(value = "SELECT * FROM public.vehicle WHERE ST_CONTAINS(:polygon, vehicle.coordinate)",
            nativeQuery = true)
    List<Vehicle> findVehicles(@Param("polygon") Polygon polygon);

}
