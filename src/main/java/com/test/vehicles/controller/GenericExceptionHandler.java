package com.test.vehicles.controller;

import com.test.vehicles.exception.VehicleNotFoundException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.Optional;

import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpStatus.NOT_FOUND;

@ControllerAdvice
public class GenericExceptionHandler extends ResponseEntityExceptionHandler {

    private static final Logger LOG = LoggerFactory.getLogger(GenericExceptionHandler.class);

    @ExceptionHandler(Exception.class)
    public ResponseEntity<?> handleException(Exception exception) {
        LOG.error("Server error", exception);

        return ResponseEntity
                .status(INTERNAL_SERVER_ERROR)
                .body(Optional.of(exception.getMessage()).orElse("Unknown server error"));
    }

    @ExceptionHandler(VehicleNotFoundException.class)
    public ResponseEntity<?> handleVehicleNotFoundException(VehicleNotFoundException exception) {
        LOG.debug("Vehicle not found", exception);

        return ResponseEntity
                .status(NOT_FOUND)
                .body(exception.getMessage());
    }
}
