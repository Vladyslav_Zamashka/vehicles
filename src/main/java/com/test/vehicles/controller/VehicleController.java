package com.test.vehicles.controller;

import com.test.vehicles.model.dto.VehicleDTO;
import com.test.vehicles.service.VehicleGeoService;
import com.test.vehicles.service.VehicleService;

import lombok.AllArgsConstructor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.List;

import static com.test.vehicles.c.C.MAX_LATITUDE;
import static com.test.vehicles.c.C.MAX_LONGITUDE;
import static com.test.vehicles.c.C.MIN_LATITUDE;
import static com.test.vehicles.c.C.MIN_LONGITUDE;

@AllArgsConstructor
@RestController
@Validated
@RequestMapping("/vehicle")
public class VehicleController {

    private static final Logger LOG = LoggerFactory.getLogger(VehicleController.class);

    private final VehicleGeoService vehicleGeoService;
    private final VehicleService vehicleService;

    @GetMapping
    public ResponseEntity<List<VehicleDTO>> getVehicles(@NotNull @Min(MIN_LATITUDE) @Max(MAX_LATITUDE) double latitude1,
                                                     @NotNull @Min(MIN_LATITUDE) @Max(MAX_LATITUDE) double latitude2,
                                                     @NotNull @Min(MIN_LONGITUDE) @Max(MAX_LONGITUDE) double longitude1,
                                                     @NotNull @Min(MIN_LONGITUDE) @Max(MAX_LONGITUDE) double longitude2) {
        LOG.info("Searching vehicles for long1: {}, long2: {}, lat1: {}, lat2: {}", longitude1, longitude2, latitude1, latitude2);

        List<VehicleDTO> vehiclesForPolygon = vehicleGeoService.findVehiclesByPolygon(latitude1, longitude1, latitude2, longitude2);

        return ResponseEntity.ok(vehiclesForPolygon);
    }

    @PostMapping
    public ResponseEntity<VehicleDTO> createVehicle(@RequestBody @Valid VehicleDTO vehicle) {
        LOG.info("Creating vehicle longitude: {}, latitude: {}", vehicle.getLongitude(), vehicle.getLatitude());

        return ResponseEntity.ok(vehicleService.saveVehicle(vehicle));
    }

    @PutMapping("/{id}")
    public ResponseEntity<VehicleDTO> updateVehicle(@RequestBody @Valid VehicleDTO vehicle, @PathVariable @NotNull @Min(0) long id) {
        LOG.debug("Updating vehicle with id: {}, longitude: {}, latitude: {}", id, vehicle.getLongitude(), vehicle.getLatitude());

        return ResponseEntity.ok(vehicleService.updateVehicle(id, vehicle));
    }
}
