package com.test.vehicles.c;

public final class C {

    public static final int MIN_LATITUDE = -90;
    public static final int MAX_LATITUDE = 90;

    public static final int MIN_LONGITUDE = -180;
    public static final int MAX_LONGITUDE = 180;

    public static final int SRID = 4326;

    private C() {}

}
