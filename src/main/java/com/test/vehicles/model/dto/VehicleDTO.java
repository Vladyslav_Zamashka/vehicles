package com.test.vehicles.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.time.LocalDateTime;

import static com.test.vehicles.c.C.MAX_LATITUDE;
import static com.test.vehicles.c.C.MAX_LONGITUDE;
import static com.test.vehicles.c.C.MIN_LATITUDE;
import static com.test.vehicles.c.C.MIN_LONGITUDE;

@Data
@Builder
@AllArgsConstructor
public class VehicleDTO {
    private long id;

    @Min(MIN_LONGITUDE)
    @Max(MAX_LONGITUDE)
    private double longitude;

    @Min(MIN_LATITUDE)
    @Max(MAX_LATITUDE)
    private double latitude;

    private LocalDateTime updated;
}
