package com.test.vehicles.model.converter;

public interface Converter<E, T> {
    E convert(T vehicle);
}
