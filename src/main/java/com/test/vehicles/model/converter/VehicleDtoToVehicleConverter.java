package com.test.vehicles.model.converter;

import com.test.vehicles.model.dto.VehicleDTO;
import com.test.vehicles.model.entity.Vehicle;

public interface VehicleDtoToVehicleConverter extends Converter<Vehicle, VehicleDTO> {

    Vehicle convert(VehicleDTO vehicle);
}
