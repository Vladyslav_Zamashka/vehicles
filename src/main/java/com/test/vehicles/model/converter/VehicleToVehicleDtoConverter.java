package com.test.vehicles.model.converter;

import com.test.vehicles.model.dto.VehicleDTO;
import com.test.vehicles.model.entity.Vehicle;

public interface VehicleToVehicleDtoConverter extends Converter<VehicleDTO, Vehicle> {

    VehicleDTO convert(Vehicle vehicle);

}
