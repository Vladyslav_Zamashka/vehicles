package com.test.vehicles.model.converter.impl;

import com.test.vehicles.model.converter.VehicleDtoToVehicleConverter;
import com.test.vehicles.model.dto.VehicleDTO;
import com.test.vehicles.model.entity.Vehicle;
import com.test.vehicles.service.GeometryService;
import com.vividsolutions.jts.geom.Point;

import lombok.AllArgsConstructor;

import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
@AllArgsConstructor
public class DefaultVehicleDtoToVehicleConverter implements VehicleDtoToVehicleConverter {

    private final GeometryService geometryService;

    @Override
    public Vehicle convert(VehicleDTO vehicle) {
        Point vehicleCoordinate = geometryService.createPoint(vehicle.getLongitude(), vehicle.getLatitude());

        return Vehicle.builder()
                .vehicleCoordinate(vehicleCoordinate)
                .updated(LocalDateTime.now())
                .build();
    }
}
