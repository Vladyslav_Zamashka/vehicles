package com.test.vehicles.model.converter.impl;

import com.test.vehicles.model.converter.VehicleToVehicleDtoConverter;
import com.test.vehicles.model.dto.VehicleDTO;
import com.test.vehicles.model.entity.Vehicle;
import com.vividsolutions.jts.geom.Point;

import org.springframework.stereotype.Component;

@Component
public class DefaultVehicleToVehicleDtoConverter implements VehicleToVehicleDtoConverter {

    @Override
    public VehicleDTO convert(Vehicle vehicle) {
        Point point = vehicle.getVehicleCoordinate();

        return VehicleDTO.builder()
                .id(vehicle.getId())
                .updated(vehicle.getUpdated())
                .longitude(point.getX())
                .latitude(point.getY())
                .build();
    }
}
