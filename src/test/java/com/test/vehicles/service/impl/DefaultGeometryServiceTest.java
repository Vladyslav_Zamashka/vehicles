package com.test.vehicles.service.impl;

import com.test.vehicles.service.GeometryService;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.geom.Polygon;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static com.vividsolutions.jts.geom.Coordinate.X;
import static com.vividsolutions.jts.geom.Coordinate.Y;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class DefaultGeometryServiceTest {

    private static final double LONGITUDE_1 = 10.222;
    private static final double LATITUDE_1 = 12.222;

    private static final double LONGITUDE_2 = 20.222;
    private static final double LATITUDE_2 = 22.222;

    @Autowired
    private GeometryService defaultGeometryService;

    @Test
    public void shouldCreatePointFromCoordinates() {
        Point point = defaultGeometryService.createPoint(LONGITUDE_1, LATITUDE_1);

        assertEquals(point.getX(), LONGITUDE_1);
        assertEquals(point.getY(), LATITUDE_1);
    }

    @Test
    public void shouldCreatePolygonFromCoordinates() {
        Polygon polygon = defaultGeometryService.createRectanglePolygon(LONGITUDE_1, LONGITUDE_2, LATITUDE_1, LATITUDE_2);

        assertEquals(polygon.getCoordinates()[0].getOrdinate(X), LONGITUDE_1);
        assertEquals(polygon.getCoordinates()[0].getOrdinate(Y), LATITUDE_1);

        assertEquals(polygon.getCoordinates()[1].getOrdinate(X), LONGITUDE_1);
        assertEquals(polygon.getCoordinates()[1].getOrdinate(Y), LATITUDE_2);

        assertEquals(polygon.getCoordinates()[2].getOrdinate(X), LONGITUDE_2);
        assertEquals(polygon.getCoordinates()[2].getOrdinate(Y), LATITUDE_2);

        assertEquals(polygon.getCoordinates()[3].getOrdinate(X), LONGITUDE_2);
        assertEquals(polygon.getCoordinates()[3].getOrdinate(Y), LATITUDE_1);

        assertEquals(polygon.getCoordinates()[4].getOrdinate(X), LONGITUDE_1);
        assertEquals(polygon.getCoordinates()[4].getOrdinate(Y), LATITUDE_1);
    }
}