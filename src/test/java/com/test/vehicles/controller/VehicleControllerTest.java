package com.test.vehicles.controller;

import com.test.vehicles.model.dto.VehicleDTO;
import com.test.vehicles.service.VehicleGeoService;
import com.test.vehicles.service.VehicleService;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.time.LocalDateTime;
import java.util.Collections;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class VehicleControllerTest {

    private static final double LONGITUDE_1 = 10.1;
    private static final double LONGITUDE_2 = 20.2;

    private static final double LATITUDE_1 = 30.1;
    private static final double LATITUDE_2 = 40.2;

    private static final long VEHICLE_DTO_ID = 1;
    private static final double VEHICLE_DTO_LONGITUDE = 2.2;
    private static final double VEHICLE_DTO_LATITUDE = 1.1;
    private static final LocalDateTime VEHICLE_DTO_UPDATED = LocalDateTime.now();
    private static final VehicleDTO vehicleDTO = new VehicleDTO(VEHICLE_DTO_ID,
            VEHICLE_DTO_LONGITUDE, VEHICLE_DTO_LATITUDE, VEHICLE_DTO_UPDATED);

    private static final JSONObject postVehicle = new JSONObject();

    @Autowired
    private MockMvc mvc;
    @MockBean
    private VehicleGeoService vehicleGeoService;
    @MockBean
    private VehicleService vehicleService;

    @BeforeAll
    public static void setUp() throws JSONException {
        postVehicle.put("latitude", VEHICLE_DTO_LATITUDE);
        postVehicle.put("longitude", VEHICLE_DTO_LONGITUDE);
    }

    @Test
    public void shouldReturnResponseWithVehiclesInRectangle() throws Exception {
        given(vehicleGeoService.findVehiclesByPolygon(LATITUDE_1, LONGITUDE_1, LATITUDE_2, LONGITUDE_2))
                .willReturn(Collections.singletonList(vehicleDTO));

        mvc.perform(get("/vehicle?longitude1=10.1&longitude2=20.2&latitude1=30.1&latitude2=40.2")
                .accept(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id").value(VEHICLE_DTO_ID))
                .andExpect(jsonPath("$[0].latitude").value(VEHICLE_DTO_LATITUDE))
                .andExpect(jsonPath("$[0].longitude").value(VEHICLE_DTO_LONGITUDE));
    }

    @Test
    public void shouldReturnResponseWithCreatedVehicleDto() throws Exception {
        given(vehicleService.saveVehicle(any()))
                .willReturn(vehicleDTO);

        checkResponse(mvc.perform(post("/vehicle")
                .contentType(APPLICATION_JSON)
                .content(postVehicle.toString())
                .accept(APPLICATION_JSON)));
    }

    @Test
    public void shouldReturnResponseWithUpdatedVehicleDto() throws Exception {
        given(vehicleService.updateVehicle(anyLong(), any(VehicleDTO.class)))
                .willReturn(vehicleDTO);

        checkResponse(mvc.perform(put("/vehicle/" + VEHICLE_DTO_ID)
                .contentType(APPLICATION_JSON)
                .content(postVehicle.toString())
                .accept(APPLICATION_JSON)));
    }

    private void checkResponse(ResultActions result) throws Exception {
        result
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(VEHICLE_DTO_ID))
                .andExpect(jsonPath("$.latitude").value(VEHICLE_DTO_LATITUDE))
                .andExpect(jsonPath("$.longitude").value(VEHICLE_DTO_LONGITUDE));
    }
}